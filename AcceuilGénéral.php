<?php
session_start();

?>

<!DOCTYPE html>
<html>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<head>
		<!-- En-tête de la page -->
		<meta charset="utf-8" />
		<link rel="stylesheet"  href="styleConnexion.css">
		<title>CONNEXION</title>	
		<!-- Cette balise affiche le titre de la page -->

    <script> 
    var _hidediv = null;
    function showdiv(id) {
    if(_hidediv)
        _hidediv();
    var div = document.getElementById(id);
    div.style.display = 'block';
    _hidediv = function () { div.style.display = 'none'; };
  }
</script>
	</head>

	<body background="C:\Users\dieyy\Desktop\Editeur de Texte\Images du site.jpg">

		<header>
			<h1>Bienvenue à toi!!!</h1>
		</header>


		<nav>
		<nav class="navbar navbar-dark bg-dark">
 			<form class="form-inline">
    			<a class="btn btn-success" href="#P" role="button">Projets Réalisés</a>
          <a class="btn btn-light" href="#C" role="button">Classement</a>
          <a class="btn btn-success" href="#V" role="button">Voter</a>
          <a class="btn btn-light" href="#A" role="button">Ajouter Projet</a>
          <a class="btn btn-success" href="#E" role="button">Elections</a>
  			</form>
        <form action="déconnexion.php" autocomplete="on" method="post"> <input type="submit" id="leave" name="Déconnexion" value="Déconnexion"/> </form>
		</nav>
			
		</nav>


		<section>

			<p>
			<section>

<!-----------------------------------PARTIE AFFICHAGE DES POSTERS------------------------------------------------->

				<div class="container">
  					<nav class="navbar navbar-dark bg-dark">
    					<a class="navbar-brand" href="#" id="P">Posters Réalisés</a>
  					</nav>
            <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
              <div class="carousel-inner">
                <div class="carousel-item active">
                  <img src="C:\wamp64\www\site_web\Images du site\ping1.jpg" class="d-block w-100" alt="Image_Du_Ping_1">
                </div>
                <div class="carousel-item">
                  <img src="C:\wamp64\www\site_web\Images du site\ping3.jpg" class="d-block w-100" alt="Image_Du_Ping_3">
                </div>
                <div class="carousel-item">
                  <img src="C:\wamp64\www\site_web\Images du site\ping4.jpg" class="d-block w-100" alt="Image_Du_Ping_4">
                </div>
              </div>
            <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
              <span class="carousel-control-prev-icon" aria-hidden="true"></span>
              <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
              <span class="carousel-control-next-icon" aria-hidden="true"></span>
              <span class="sr-only">Next</span>
            </a>
            </div>
				</div>

<!-----------------------------------PARTIE AJOUT D'UN CLASSEMENT------------------------------------------------->
				<div class="container">
  					<nav class="navbar navbar-dark bg-dark">
    					<a class="navbar-brand" href="#" id="C">Classement</a>
  					</nav>
<?php

$mysqli = new mysqli('localhost','root','','database');

 if ($mysqli->connect_errno) 
{
  echo "Echec lors de la connexion à MySQL : (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
}
else
{
$rech_class=$mysqli->query("SELECT * FROM ajout_projet ORDER BY Nbr_Vote DESC " );
  if(!$rech_class)
  {
    die("Echec de la requête SQL :".$mysqli->error);
  }
  elseif($rech_class->num_rows == 0)
    {
      echo "Echec lors de la connexion à la bdd/Aucun resultat";
    }
      else
      {
        while($tuple=$rech_class->fetch_assoc())
        {
          echo '<p>Auteur: '.htmlentities($tuple['Auteur_login']).'</p>';
          echo '<p>Nombre de Vote: '.htmlentities($tuple['Nbr_Vote']).'</p>';
        }
      }
}


?>
				</div>

<!-----------------------------------PARTIE VOTER----------------------------------------------------------->
      <!--<div id="id_div_1" style="display:none;"> --->
      <div id="1er"     style="display: none;">
 	      <div class="container">
  					<nav class="navbar navbar-dark bg-dark">
    					<a class="navbar-brand" href="#" id="V">Voter</a>
  					</nav>
          <form action="ajout_vote_dans_bdd.php" method="post">
            <table>
                <tr>
                    <td><label for="Auteur_login1"><strong>Auteur du Projet</strong></label></td>
                    <td><input type="text" name="Auteur_login1" id="Auteur_login1"/></td>
                 </tr>
            </table>
            <input type="submit"  value="Voter pour le projet de cet Auteur"/>
          </form>
				</div>
      </div>

<!-----------------------------------PARTIE AJOUT D'UN POSTER------------------------------------------------->
				<div class="container">
  					<nav class="navbar navbar-dark bg-dark">
    					<a class="navbar-brand" href="#" id="A">Ajout Poster</a>
  					</nav>
            <form action="Ajouter_Projet.php" method="post">
              <table>           
                <tr>
                    <td><label for="Nom_Projet"><strong>Nom du Projet</strong></label></td>
                    <td><input type="text" name="Nom_Projet" id="Nom_Projet"/></td>
                </tr>
                <tr>              
                    <td><label for="Auteur_login"><strong>Auteur Projet</strong></label></td>
                    <td><input type="text" name="Auteur_login" id="Auteur_login"/></td>              
                </tr>
                <tr>              
                    <td><label for="Auteur_login"><strong>Lien de l'image</strong></label></td>
                    <td><input type="text" name="LIEN_Im_projet" id="LIEN_Im_projet"/></td>             
                </tr>
              </table>
              <input type="submit"  value="Ajouter le Projet"/>
            </form>
  				
				</div>
<!-----------------------------------PARTIE DEBUT OU ARRET ELECTIONS------------------------------------------------->
				<div class="container">
  					<nav class="navbar navbar-dark bg-dark">
    					<a class="navbar-brand" href="#" id="E">Elections</a>
  					</nav>
            <!--
            <form action="Arret_Election.php" autocomplete="on" method="post"> 
              <input type="submit" id="stop" name="Arrêter Elections" value="Arrêter Elections">
            </form>
            -->

          <a  onclick="showdiv('1er'); " href="#">Débuter Les Elections</a>
          <br>
          <a  onclick="_hidediv('1er'); " href="#">Arrêter Les Elections</a>



				</div>

			</section>

			</p>
		</section>
		

		<footer>
			<p>Copyright ESIGELEC - Tous droits réservés</p>
			
			<img src="C:\wamp64\www\site_web\Images du site\Logo_ESIGELEC.jpg" class="rounded float-left" alt="logo de l'ESIGELEC"/>
		</footer>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
	</body>
</html>