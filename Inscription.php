<?php
session_start();

?>
<!DOCTYPE html>
<html>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<head>
		<!-- En-tête de la page -->
		<meta charset="utf-8" />
		<link rel="stylesheet"  href="styleInscription.css">
		<title>INSCRIPTION</title>	
		<!-- Cette balise affiche le titre de la page -->
	</head>

	<body >

		<header>
			<h1>Bienvenue à toi!!! Suis les instructions pour t'inscrire...</h1>
		</header>

		
		<nav>
			<h2>Navigation</h2>
			<div class="btn-group-vertical">
  			<a class="btn btn-primary" href="Connexion.php" role="button">Connexion</a> 
  			<a class="btn btn-primary" href="test.php" role="button">Acceuil</a> 
		</nav>


		<section>
			<h2>DESCRIPTIF</h2>			<!-- les balises h sont des balises permettant de mettre les titres suivant leur importance -->
			<p>Vous avez choisi de vous inscrire sur notre plateforme (super) nous vous en remercions!!!<br/> 
			Pour finaliser votre inscription veuillez rentrer un identifiant et votre mot de passe dans les onglets spécifiés.
			</p>

			<form action="InscriptionTest1.php" method="post">
         
         		<table>
            
            		<tr>
               
               			<td><label for="login"><strong>Nom de compte</strong></label></td>
               			<td><input type="text" name="login" id="login"/></td>
               
           			 </tr>
            
            		<tr>
               
               			<td><label for="pass"><strong>Mot de passe</strong></label></td>
               			<td><input type="password" name="pass" id="pass"/></td>
               
            		</tr>

            		<p>
            			<label for="fonction">Fonction</label>
            			<select name="fonction" id="fonction">
            				<option value="1"> Administrateur</option>
            				<option value="2"> Utilisateur</option>
            			</select>
            		</p>

            
         		</table>
         
         		<input type="submit" name="register" value="S'inscrire"/>
      
      		</form>


		</section>
		

		<footer>
			<p>Copyright ESIGELEC - Tous droits réservés</p>
			<br/>
			<br/>
			<br/>
			<br/>
			<br/>
			<br/>
			<br/>
			<img src="C:\Users\dieyy\Desktop\Editeur de Texte\Images du site\Logo_ESIGELEC.jpg" class="rounded float-left" alt="logo de l'ESIGELEC"/>
		</footer>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
	</body>
</html>
